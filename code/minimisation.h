#ifndef MINIMISATION_H
#define MINIMISATION_H

#define ETATS_MAX 10
#define NB_COL 5
#define COUPLES_MAX 100

typedef struct {
	int num;
	int a;
	int b;
	bool final;
} Etat;

typedef struct {
	Etat etat1;
	Etat etat2;
} CoupleEtats;

void afficheFile (const std::queue<CoupleEtats>& file);
void afficherADF (Etat AFD[ETATS_MAX], int nbEtats);
CoupleEtats coupleAntecedent (char var, Etat AFD[ETATS_MAX], int nbEtats, CoupleEtats couple_file);

#endif
