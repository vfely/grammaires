#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <fstream>
#include <string>
#include <sstream>

using namespace std;
// je creer un tableau  pour l'automate

/*focntion trouve sur internet */
string IntEnString(int entier)
{
	// créer un flux de sortie 
	ostringstream oss;
	// écrire un nombre dans le flux 
	oss << entier;
	// récupérer une chaîne de caractères 

return oss.str();
}


 string addlog(string T1, string T2){
	
	for (int i = 3; i >= 0; i--){
	
		if (T1[i] == '0' && T2[i] == '0'){ T1[i]='0'; }
		
		if (T1[i] == '1' && T2[i] == '1'){ T1[i] ='0'; T1[i - 1] ='1'; }
		
		if ((T1[i] == '1' && T2[i] == '0') || (T1[i] == '0' && T2[i] == '1')){ T1[i] ='1'; }
	}
	
	return T1;
}


int main(){


	string ligne, contenu;
	string TabAutomate[4][4], NewTabAutomate[5][10];
	int newa = 0, newb = 0;
	// 
	int tailleNT;
	ifstream automate("automate.txt", ios::in);  // on ouvre le fichier en lecture

	if (!automate)  // si l'ouverture n'a pas réussi
	{

		cerr << endl << "Impossible d'ouvrir le fichier !" << endl;
		return 0;
	}
	cout << endl <<" -----------------  PARTIE I : TRANSFORMATION -------------" <<endl  <<endl;
	int i, j;
	for (i = 0; i < 4; i++){
		for (j = 0; j < 4; j++)  // tant que l'on peut mettre la ligne dans "ligne"
		{
			// dans le buffer , on aura Etat actuel | A | B | Terminaux
			if (getline(automate, ligne)){
				TabAutomate[i][j] = ligne;
				ligne.clear();
			}
		}
	}

	// on affiche le resultat 

	cout << "etat	|	a	|	b	| fini? " << endl;
	cout << "-------------------------------------------------" << endl;
	for (i = 0; i < 4; i++){

		cout << "" << TabAutomate[i][0] << "	|	" << TabAutomate[i][1] << "	|	" << TabAutomate[i][2] << "	|" << TabAutomate[i][3] << endl;
	}

	// LA PARTI ----- Translation 

	// on va commencer a la remplir le new automate

	// on remplie le premiere

	//condition on n'arrete pas tant que on trouve des nouveaux etats 
	tailleNT = 1;
	NewTabAutomate[0][0] = "0";
	NewTabAutomate[1][0] = "1000";
	// indice etat actuel
	int etatactuel = 0;
	// indice des nouveaux etat
	int indNetat = 1;
	string sommea = "0000";
	string sommeb = "0000";
	 i = 0;
	 int nbetat=0;
	
	do{

		/*Probleme addition logique ne marche pas  */
		
		contenu = NewTabAutomate[1][etatactuel];
		// bool d'ecrit pour savoir , si on continue ou non 
	
		cout << "etat actuel est " << etatactuel;
		// on parcours les 4 etat
		for (i = 0; i < 4; i++){

			
			//  je teste si pour la sequence il y a des etat vrai alors on fait regarde dans l'automate la valeur de la sequence et on fait la somme logique avec les autres sequence
			if (contenu[i]=='1'){
				/// NB :si on veut faire la solution pour plusieur variable voir pour une boucle avec le 1er [] a incrementer  
				 sommea=addlog(sommea, TabAutomate[i][1]);// pour le A
				
				 sommeb=addlog(sommeb, TabAutomate[i][2]);// pour le B
			
				 
			}
		}

		// on regarde si dans le nouveau tableau on a deja cette etat sinon 
		for (i = 0; i < 4; i++){
			
			if (NewTabAutomate[1][i].compare(sommea)==0){
	//			cout << " tab " << NewTabAutomate[1][i] << " : " << endl;
				// alors on donne l'etat a A 
				cout << endl << "	etat existant	" << endl;
				
				NewTabAutomate[2][etatactuel] =IntEnString( i);
				
				newa = 0;
				sommea= "0000";
				}
			// de meme mais pour B
		
			if (NewTabAutomate[1][i].compare(sommeb)==0)
			{
		//		cout << " tab " << NewTabAutomate[1][i] << " : " << endl;
				cout << endl << "	etat existant " << endl << endl;
				cout << " on ajoute " << IntEnString(i) << "  en [3][" << etatactuel << "]";
				
				NewTabAutomate[3][etatactuel] = IntEnString( i);
		
				newb = 0;
				sommeb = "0000";
			}
			}
			// sinon on l'ajoute
		if (i >= 4){
			//cout << "nouvel etat " << endl;
			if (sommea != "0000")
			{
				// pour A 
				//cout << endl << "		nouvelle etat trouvé " << endl;
				cout << endl << "(A)etat : " << indNetat << "	" << sommea << endl << endl;
				NewTabAutomate[1][indNetat] = sommea;
				NewTabAutomate[0][indNetat] = IntEnString(indNetat);
				NewTabAutomate[2][etatactuel] = IntEnString(indNetat);
				
				newa = 1;
				indNetat++;
				nbetat++;
				
			}
			else{
				if (sommeb != "0000"){
					// pour B
					//cout << endl << "		nouvelle etat trouvé " << endl;
					cout << endl << "(B)etat : " << indNetat << "	" << sommeb << endl << endl;
					
					// on rajoute le nouveau etat
					NewTabAutomate[1][indNetat] = sommeb;
					// on rajoute la l'indice  Nouveaux
					
					NewTabAutomate[0][indNetat] = IntEnString(indNetat);
					// on ajoute le etat que a doit atteindre , quand il est a l'etat actuels
					NewTabAutomate[3][etatactuel] = IntEnString(indNetat);
					// on a trouve un nouvel etat donc on va continuer	
					newb = 1;
					// on augmente l'indice de la liste de nouvel etat
					indNetat++;
					nbetat++;
				}
			}
		
			// on regarde si la somme du new etat ressemble a un etat deja creer
		}
		/*on regarde s'il y a un prochain etat */
		if (newb == 1 || newa == 1){
			
			sommeb = "0000";
			sommea = "0000";
			NewTabAutomate[4][etatactuel]="FALSE";
				etatactuel++;
		}
		else{ NewTabAutomate[4][etatactuel]="TRUE"; }

	//	cout << " new a = " << newa << " , newb =" << newb << endl;
		cout << "etat	|	newliste |	a	|	b	| fini? " << endl;
		cout << "----------------------------------------------------------------------------------" << endl;
		for (int i = 0; i < nbetat + 1; i++){

			cout << "" << NewTabAutomate[0][i] << "	|	" << NewTabAutomate[1][i] << "	|	" << NewTabAutomate[2][i] << "	|	" << NewTabAutomate[3][i] << "	| " << NewTabAutomate[4][i] << endl;
		}
	} while (newa!=0 || newb!=0);

	
 
//grace a un nouveau tableau 
		return 0;
}
