#include <iostream>
#include <string.h>
#include <stdio.h>
#include <queue>
#include "minimisation.h"

using namespace std;

void lectureFichier (Etat AFD[ETATS_MAX], int &nbEtats) {
	FILE * f = fopen ("AFD.txt" , "r");
	int num;
	
	int i = 0;
	while(!feof(f)) {
		fscanf(f, "%i %i %i %i\n", &(AFD[i].num), &(AFD[i].a), &(AFD[i].b), &(AFD[i].final));
		i++;
	}
	nbEtats = i;
	
	fclose (f);
}

void afficherAFD (Etat etat[ETATS_MAX], int nbEtats) {
	cout << "etat \t| etat vers a \t| etat vers b \t| final " << endl;
	
	for (int i = 0; i < nbEtats; i++) {
			cout << etat[i].num << " \t| " << etat[i].a << " \t\t| " << etat[i].b << " \t\t| " << etat[i].final << endl;
	}
}

void afficheFile (const queue<CoupleEtats>& file) {
	cout << endl;
	cout << "File : " << endl;
	queue<CoupleEtats> q = file;
    while(!q.empty())
    {
        CoupleEtats coupleEtats = q.front();
        Etat etat1 = coupleEtats.etat1;
        Etat etat2 = coupleEtats.etat2;
        
        cout << etat1.num << " " << etat2.num << endl;
        q.pop();
    }
}

CoupleEtats coupleAntecedent (char var, Etat AFD[ETATS_MAX], int nbEtats, CoupleEtats couple_file)
{
	CoupleEtats couple_antecedent;
	couple_antecedent.etat1.num = -1;
	couple_antecedent.etat2.num = -1;
	
	if (var == 'a' ) { // On cherche l'antécédent selon a
		for (int i = 0; i < nbEtats ; i++) {
			for (int j = 0; j < nbEtats; j++) {
				// antecedent de l'etat1
				if (AFD[i].a == couple_file.etat1.num)
					couple_antecedent.etat1 = AFD[i];
			
				// antecedent de l'etat2
				if (AFD[i].a == couple_file.etat2.num)
					couple_antecedent.etat2 = AFD[i];
			}
		}
	}
	
	if (var == 'b' ) { // On cherche l'antécédent selon b
		for (int i = 0; i < nbEtats ; i++) {
			for (int j = 0; j < nbEtats; j++) {
				// antecedent de l'etat1
				if (AFD[i].b == couple_file.etat1.num)
					couple_antecedent.etat1 = AFD[i];
			
				// antecedent de l'etat2
				if (AFD[i].b == couple_file.etat2.num)
					couple_antecedent.etat2 = AFD[i];
			}
		}
	}
	
	return couple_antecedent;
}

bool sont_distingue (Etat etat1, Etat etat2)
{
	if (etat1.num != etat2.num) {
		if ((etat1.final == true) && (etat2.final == false)) {
			return true;
		}
		
		if ((etat1.final == false) && (etat2.final == true)) {
			return true;
		}
	}
	
	return false;
}

int main () {
	int nbEtats;
	
	//Charge l'AFD
	Etat AFD[ETATS_MAX];
	lectureFichier(AFD, nbEtats);
	
	afficherAFD(AFD, nbEtats);
	
	// Mettre dans la file les états distingués par défaut couple état final / etat non final
	queue<CoupleEtats> file;
	CoupleEtats coupleEtats;
	char tab[ETATS_MAX][ETATS_MAX];
	
	// Pour les états distingués par défaut
	for (int i = 0; i < nbEtats ; i++) {
		for (int j = 0; j < nbEtats; j++) {
			tab[i][j] = '_';
			if (sont_distingue(AFD[i], AFD[j])) {
				coupleEtats.etat1 = AFD[i];
				coupleEtats.etat2 = AFD[j];
				file.push(coupleEtats);
				tab[i][j] = 'D';
			}
		}
	}
	
	afficheFile(file);

	// Pour les autres
	CoupleEtats couple_file;
	CoupleEtats couple_antecedent;
	
	while (!file.empty())
	{
		couple_file = file.front();
		file.pop();
		
		// Antécédent selon a
		couple_antecedent = coupleAntecedent ('a', AFD, nbEtats, couple_file);
	
		cout << "antécédent selon a de " << couple_file.etat1.num << " " << couple_file.etat2.num << endl;
		cout << "--> " << couple_antecedent.etat1.num << " " << couple_antecedent.etat2.num << endl;
		if (couple_antecedent.etat1.num != -1 && couple_antecedent.etat2.num != -1) // Si on a trouvé un antécédent pour les deux
		{
			if (tab[couple_antecedent.etat1.num][couple_antecedent.etat2.num] != 'D') // couple non marqué
			{
				tab[couple_antecedent.etat1.num][couple_antecedent.etat2.num] = 'D';
				file.push(couple_antecedent);
			}
		}
		
		// Antécédent selon b
		couple_antecedent = coupleAntecedent ('b', AFD, nbEtats, couple_file);

		cout << "antécédent selon b de " << couple_file.etat1.num << " " << couple_file.etat2.num << endl;
		cout << "--> " << couple_antecedent.etat1.num << " " << couple_antecedent.etat2.num << endl;
		if (couple_antecedent.etat1.num != -1 && couple_antecedent.etat2.num != -1) // Si on a trouvé un antécédent pour les deux
		{
			if (tab[couple_antecedent.etat1.num][couple_antecedent.etat2.num] != 'D') // couple non marqué
			{
				tab[couple_antecedent.etat1.num][couple_antecedent.etat2.num] = 'D';
				file.push(couple_antecedent);
			}
		}
		
		afficheFile(file);
	}
	
	// Affiche le tableau
	cout << "Tableau :" << endl;
	for (int i = 0; i < nbEtats ; i++) {
		for (int j = 0; j < nbEtats; j++) {
			if (i <= j)
				tab[i][j] = 'x';
				
			cout << tab[i][j] <<  " ";
		}
		cout << endl;
	}
	
	// Affiche les valeurs à _ dans le tableau
	cout << "Couples indistingués :" << endl;
	for (int i = 0; i < nbEtats ; i++) {
		for (int j = 0; j < nbEtats; j++) {
			if (tab[i][j] == '_')
				cout << i << " " << j << endl;
		}
	}
}

